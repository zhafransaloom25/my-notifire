package com.example.mynotifire.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Gedung(
        var id_gedung : String,
        var nama_gedung : String,
        var alamat_gedung : String,
        var latitude : String,
        var longitude : String,
        var status : String? = null
) :Parcelable