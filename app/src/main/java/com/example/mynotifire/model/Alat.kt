package com.example.mynotifire.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Alat(
        var id_alat : String,
        var lokasi_alat : String,
) : Parcelable