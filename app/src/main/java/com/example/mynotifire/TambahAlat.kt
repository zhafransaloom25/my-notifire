package com.example.mynotifire

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.mynotifire.model.Alat
import com.example.mynotifire.model.Gedung
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import org.w3c.dom.Comment

class TambahAlat : AppCompatActivity() {

    companion object {
        const val EXTRA_DETAIl = "extra_detail"
    }
//    private var barang : Gedung? = null
    lateinit var idAlat : EditText
    lateinit var databaseReference : DatabaseReference
    lateinit var lokasiAlat : EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_alat)

//        barang = intent.getParcelableExtra(EXTRA_DETAIl) as Gedung

        val key = intent.getStringExtra("KEY")
        Toast.makeText(this@TambahAlat, "$key", Toast.LENGTH_SHORT).show()

        val daftarAlat = findViewById<Button>(R.id.daftarAlat)

        daftarAlat.setOnClickListener {
            idAlat = findViewById<EditText>(R.id.edtIdAlat)
            lokasiAlat = findViewById<EditText>(R.id.edtLokasiAlat)

            var id = idAlat.text.toString().trim()
            var lokasialat = lokasiAlat.text.toString().trim()

            val alat = Alat(id, lokasialat)
            databaseReference = FirebaseDatabase.getInstance().getReference("users")
            databaseReference.child(FirebaseAuth.getInstance().currentUser!!.uid).child("gedung").child(key).child("alat").child(id).setValue(alat)
            val intent = Intent(this@TambahAlat, ListAlat::class.java)
            startActivity(intent)

        }
    }
}