package com.example.mynotifire.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mynotifire.ListAlat
import com.example.mynotifire.R
import com.example.mynotifire.model.Alat
import com.example.mynotifire.model.Gedung

class AlatAdapter(val context: Context, val alatlist: ArrayList<Alat>) :
        RecyclerView.Adapter<AlatAdapter.Holder>() {

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder?.bind(alatlist[position], context)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_alat, parent, false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return alatlist.size
    }


    inner class Holder(view: View?) : RecyclerView.ViewHolder(view!!) {
        val posisiAlat = view?.findViewById<TextView>(R.id.txv_destination)
        val alamatGedung = view?.findViewById<TextView>(R.id.txv_dest_desc)

        fun bind(bal: Alat, context: Context) {
            posisiAlat!!.text = bal.lokasi_alat
            //  recordDeleteImageView.imageb
        }
    }
}