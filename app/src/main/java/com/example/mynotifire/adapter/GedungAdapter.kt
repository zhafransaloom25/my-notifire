package com.example.mynotifire.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mynotifire.ListAlat
import com.example.mynotifire.ListFragment
import com.example.mynotifire.R
import com.example.mynotifire.model.Gedung


class GedungAdapter(val context: Context, val gedunglist: ArrayList<Gedung>) :
        RecyclerView.Adapter<GedungAdapter.Holder>() {

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder?.bind(gedunglist[position], context)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_gedung, parent, false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return gedunglist.size
    }


    inner class Holder(view: View?) : RecyclerView.ViewHolder(view!!) {
        val namaGedung = view?.findViewById<TextView>(R.id.txv_namagedung)
        val alamatGedung = view?.findViewById<TextView>(R.id.txv_lokasi_gedung)

        fun bind(bal: Gedung, context: Context) {
            namaGedung!!.text = bal.nama_gedung
            alamatGedung!!.text = bal.alamat_gedung

            itemView.setOnClickListener {
                val intent = Intent(context, ListAlat::class.java)
                intent.putExtra("KEY", bal.id_gedung)
                context.startActivity(intent)
            }
            //  recordDeleteImageView.imageb
        }
    }
}