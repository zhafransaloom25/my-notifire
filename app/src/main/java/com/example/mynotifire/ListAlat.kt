package com.example.mynotifire

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mynotifire.adapter.AlatAdapter
import com.example.mynotifire.adapter.GedungAdapter
import com.example.mynotifire.model.Alat
import com.example.mynotifire.model.Gedung
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class ListAlat : AppCompatActivity() {

    var databaseReference: DatabaseReference? = null
    var databaseRf : DatabaseReference? = null
    var mRecyclerView : RecyclerView? =null
    lateinit var alatList : ArrayList<Alat>
    lateinit var auth : FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_alat)

        alatList = arrayListOf()

        val key = intent.getStringExtra("KEY") as String

        Toast.makeText(this@ListAlat, key, Toast.LENGTH_SHORT).show()

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab!!.setOnClickListener {
            val intent = Intent(this@ListAlat, TambahAlat::class.java)
            intent.putExtra("KEY", key)
            startActivity(intent)
        }

        auth = FirebaseAuth.getInstance()
        mRecyclerView = findViewById(R.id.rcView)
        mRecyclerView!!.setHasFixedSize(true)
        mRecyclerView!!.layoutManager = LinearLayoutManager(this)

        databaseReference = FirebaseDatabase.getInstance().getReference("users").child(auth.currentUser!!.uid).child("gedung").child(key).child("alat")

        databaseReference!!.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(p0: DataSnapshot) {
                for(h in p0.children){

                    val id_alat = h.child("id_alat").value.toString()
                    val lokasi_alat = h.child("lokasi_alat").value.toString()

                    alatList.add(
                            Alat( id_alat, lokasi_alat
                            )
                    )

                    Log.d("alat","$alatList")
                }
                val adapter = AlatAdapter(this@ListAlat, alatList)
                mRecyclerView!!.adapter = adapter
            }

        })

        return
    }
}