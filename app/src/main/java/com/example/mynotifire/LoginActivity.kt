package com.example.mynotifire

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import org.w3c.dom.Text

class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = Firebase.auth

        val btn = findViewById<TextView>(R.id.btn_daftar)
        btn.setOnClickListener {
            val intent = Intent(this@LoginActivity, RegistrationActivity::class.java)
            startActivity(intent)
        }

        val btnLogin = findViewById<Button>(R.id.cirLoginButton)
        btnLogin.setOnClickListener {
            doLogin()
        }
    }

    private fun doLogin() {

        val editTextEmail = findViewById<TextView>(R.id.editTextEmail)
        val editTextPassword = findViewById<TextView>(R.id.editTextPassword)

        if(editTextEmail.text.isEmpty()){
            editTextEmail.setError("Email Tidak Boleh Kosong!")
            editTextEmail.requestFocus()
            return
        }

        if(editTextPassword.text.isEmpty()){
            editTextPassword.setError("Password Tidak Boleh Kosong!")
            editTextPassword.requestFocus()
            return
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.text.toString()).matches()){
            editTextEmail.setError("Masukkan format email yang benar!")
            editTextEmail.requestFocus()
            return
        }

        auth.signInWithEmailAndPassword(editTextEmail.text.toString(), editTextPassword.text.toString())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        val user = auth.currentUser
                        updateUI(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(baseContext, "Login Gagal.",
                                Toast.LENGTH_SHORT).show()
                        updateUI(null)
                        // ...
                    }

                    // ...
                }
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun updateUI(currentUser : FirebaseUser?){

        if(currentUser !=null){
            if(currentUser.isEmailVerified){
                startActivity(Intent(this@LoginActivity, TabActivity::class.java))
                finish()
            }else{
                Toast.makeText(baseContext, "Tolong verifikasi alamat email anda.",
                        Toast.LENGTH_SHORT).show()
            }
        }else{
            Toast.makeText(baseContext, "Login Gagal.",
                    Toast.LENGTH_SHORT).show()
        }
    }
}