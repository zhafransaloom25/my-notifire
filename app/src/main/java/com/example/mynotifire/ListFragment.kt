package com.example.mynotifire

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mynotifire.adapter.GedungAdapter
import com.example.mynotifire.model.Gedung
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase

class ListFragment : Fragment() {
    var fragmentView : View? = null
    lateinit var databaseReference: DatabaseReference
    var mRecyclerView : RecyclerView? =null
    lateinit var gedungList : ArrayList<Gedung>
    lateinit var auth : FirebaseAuth

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        fragmentView= LayoutInflater.from(activity).inflate(R.layout.fragment_list, container, false)

        gedungList = arrayListOf<Gedung>()

        val fab = fragmentView?.findViewById<FloatingActionButton>(R.id.fab)
        fab!!.setOnClickListener {
            val intent = Intent(context, MapsTambah::class.java)
            startActivity(intent)
        }
        auth = FirebaseAuth.getInstance()
        mRecyclerView = fragmentView?.findViewById(R.id.rcView)
        mRecyclerView!!.setHasFixedSize(true)
        mRecyclerView!!.layoutManager = LinearLayoutManager(context)


        databaseReference = FirebaseDatabase.getInstance().getReference("users").child(auth.currentUser!!.uid).child("gedung")

        databaseReference.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(p0: DataSnapshot) {
                    for(h in p0.children){

                        val id_gedung = h.child("id_gedung").value.toString()
                        val nama_gedung = h.child("nama_gedung").value.toString()
                        val alamat_gedung = h.child("alamat_gedung").value.toString()
                        val latitude = h.child("latitude").value.toString()
                        val longitude = h.child("longitue").value.toString()
                        val status = h.child("status").value.toString()

                        gedungList.add(
                                Gedung(
                                        id_gedung,nama_gedung,alamat_gedung,latitude,longitude, "0"
                                )
                        )

                        Log.d("tampil","$h")
                    }
                    val adapter = GedungAdapter(context!!, gedungList)
                    mRecyclerView!!.adapter = adapter
            }

        })

        return fragmentView
    }

}