package com.example.mynotifire

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import androidx.appcompat.app.AppCompatActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        SystemClock.sleep(3000)
        val loginIntent = Intent(this@SplashActivity, LoginActivity::class.java)
        startActivity(loginIntent)
        finish()
    }
}