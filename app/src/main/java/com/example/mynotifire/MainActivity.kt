package com.example.mynotifire

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*
import kotlin.jvm.Throws

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        connect()
    }

    fun connect() {
        val clientId = MqttClient.generateClientId()
        val client = MqttAndroidClient(this.applicationContext, "tcp://broker.mqttdashboard.com",
                clientId)
        val options = MqttConnectOptions()
        options.mqttVersion = MqttConnectOptions.MQTT_VERSION_3_1
        options.isCleanSession = false
        try {
            val token = client.connect(options)
            token.actionCallback = object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {
                    // We are connected
                    Log.d("file", "OnSuccess")
                    subscribe(client, "notifire/sensorAsap")
                    subscribe(client, "notifire/sensorApi")
                    subscribe(client, "data/api")
                    subscribe(client, "data/asap")
                    client.setCallback(object : MqttCallback {
                        var textApi = findViewById<View>(R.id.subApi) as TextView
                        var textAsap = findViewById<View>(R.id.subAsap) as TextView
                        override fun connectionLost(cause: Throwable) {}

                        @Throws(Exception::class)
                        override fun messageArrived(topic: String, message: MqttMessage) {
                            Log.d("file", message.toString())
                            if (topic == "data/api") {
                                textApi.text = message.toString()
                            }
                            if (topic == "data/asap") {
                                textAsap.text = message.toString()
                            }
                        }

                        override fun deliveryComplete(token: IMqttDeliveryToken) {}
                    })
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    Log.d("file", "onFailure")
                }
            }
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    fun subscribe(client: MqttAndroidClient, topic: String?) {
        val qos = 1
        try {
            val subToken = client.subscribe(topic, qos)
            subToken.actionCallback = object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {}
                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {}
            }
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }
}