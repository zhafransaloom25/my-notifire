package com.example.mynotifire

import android.content.Intent
import android.location.Address
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.example.mynotifire.model.Gedung
import com.example.mynotifire.model.Users
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.layout_registration_alat.*
import kotlinx.android.synthetic.main.layout_registration_gedung.*

class TambahGedung : AppCompatActivity() {

    companion object{
        const val EXTRA_CREATE = "extra_create"
    }

    private lateinit var destination : Address

    lateinit var databaseReference : DatabaseReference
    lateinit var edtIdGedung : EditText
    lateinit var edtNamaGedung : EditText
    lateinit var edtLokasiGedung : EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_gedung)

        val daftarGedung = findViewById<Button>(R.id.daftarGedung)
        destination = intent.getParcelableExtra(EXTRA_CREATE) as Address

        edtLatitude.setText(destination.latitude.toString())
        edtLongitude.setText(destination.longitude.toString())

        daftarGedung.setOnClickListener {

            edtIdGedung = findViewById<EditText>(R.id.edtIdGedung)
            edtNamaGedung = findViewById<EditText>(R.id.edtNamaGedung)
            edtLokasiGedung = findViewById<EditText>(R.id.edtLokasiGedung)

            var nama = edtNamaGedung.text.toString().trim()
            var id =edtIdGedung.text.toString().trim()
            var alamat = edtLokasiGedung.text.toString().trim()
            var latitude = edtLatitude.text.toString().trim()
            var longitude = edtLongitude.text.toString().trim()

            databaseReference = FirebaseDatabase.getInstance().getReference("users")
            val gedung = Gedung(id,nama, alamat, latitude, longitude, "0")
            databaseReference.child(FirebaseAuth.getInstance().currentUser!!.uid).child("gedung").child(id).setValue(gedung)
            val intent = Intent(this@TambahGedung, TabActivity::class.java)

            startActivity(intent)

        }

    }
}