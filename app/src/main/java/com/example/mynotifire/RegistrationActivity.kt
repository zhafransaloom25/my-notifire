package com.example.mynotifire

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mynotifire.model.Users
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*


class RegistrationActivity : AppCompatActivity() {

    lateinit var m_Database : FirebaseDatabase
    private lateinit var auth: FirebaseAuth
    lateinit var databaseReference : DatabaseReference
    lateinit var editTextEmail : EditText
    lateinit var editTextNama : EditText
    lateinit var editTextPassword : EditText
    lateinit var editTextUsername : EditText
    var s_persistenceInitialized = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        auth = FirebaseAuth.getInstance()

        editTextEmail = findViewById<EditText>(R.id.edtEmail)
        editTextNama = findViewById<EditText>(R.id.edtNama)
        editTextUsername = findViewById<EditText>(R.id.edtUsername)
        editTextPassword = findViewById<EditText>(R.id.edtPassword)


        val btnLogin = findViewById<TextView>(R.id.btnLogin)
        btnLogin.setOnClickListener {
            val intent = Intent(this@RegistrationActivity, LoginActivity::class.java)
            startActivity(intent)
        }

        val btnRegister = findViewById<Button>(R.id.btnRegister)
        btnRegister.setOnClickListener {
            signUpUser()
        }

    }

    fun signUpUser(){

        val email = editTextEmail.text.toString().trim()
        val nama = editTextNama.text.toString().trim()
        val username = editTextUsername.text.toString().trim()
        val password = editTextPassword.text.toString().trim()
        if(email.isEmpty()){
            editTextEmail.setError("Email Tidak Boleh Kosong!")
            editTextEmail.requestFocus()
            return
        }
        if(nama.isEmpty()){
            editTextNama.setError("Nama Tidak Boleh Kosong!")
            editTextNama.requestFocus()
            return
        }
        if(username.isEmpty()){
            editTextUsername.setError("Username Tidak Boleh Kosong!")
            editTextUsername.requestFocus()
            return
        }
        if(password.isEmpty()){
            editTextPassword.setError("Password Tidak Boleh Kosong!")
            editTextPassword.requestFocus()
            return
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            editTextEmail.setError("Masukkan format email yang benar!")
            editTextEmail.requestFocus()
            return
        }



        auth.createUserWithEmailAndPassword(email,  password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        databaseReference = FirebaseDatabase.getInstance().getReference("users")
                                        val users = Users(nama, email, username, password)
                        val id = databaseReference.push().key
                        databaseReference.child(FirebaseAuth.getInstance().currentUser!!.uid).setValue(users)
                        
                        val user = auth.currentUser
                        user?.sendEmailVerification()

                        Toast.makeText(this@RegistrationActivity, "Pendaftaran berhasil", Toast.LENGTH_LONG).show()
                                                        val intent = Intent(this@RegistrationActivity, LoginActivity::class.java)
                                                        startActivity(intent)
                                                        finish()

                    } else {
                        Toast.makeText(this@RegistrationActivity, "Pendaftaran Akun Gagal! Coba lagi nanti", Toast.LENGTH_LONG).show()
                        Log.d("kenapa", task.exception!!.message)

                    }

                    // ...
                }
    }

//    public override fun onStart() {
//        super.onStart()
//        // Check if user is signed in (non-null) and update UI accordingly.
//        val currentUser = auth.currentUser
//    }

}